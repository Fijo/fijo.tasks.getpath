﻿using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using FijoCore.Infrastructure.LightContrib.Module.AlgorithmProvider;

namespace Fijo.Tasks.GetPath.Provider.Algorithm {
	public class FilterProvider : InjectionAlgorithmProvider<IFilter, FilterRule> {
		#region Overrides of InjectionAlgorithmProviderBase<IFilter,FilterRule>
		protected override FilterRule KeySelector(IFilter algorithm) {
			return algorithm.For;
		}
		#endregion
	}
}