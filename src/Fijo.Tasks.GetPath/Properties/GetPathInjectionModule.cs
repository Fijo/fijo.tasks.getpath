using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Provider;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Filters;
using Fijo.Tasks.GetPath.Interfaces;
using Fijo.Tasks.GetPath.Provider.Algorithm;
using Fijo.Tasks.GetPath.Service;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Tasks.GetPath.Properties {
	public class GetPathInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IAlgorithmProvider<IFilter, FilterRule>>().To<FilterProvider>().InSingletonScope();
			kernel.Bind<IFinder<IFilter, FilterRule>>().To<Finder<IFilter, FilterRule>>().InSingletonScope();
			kernel.Bind<IFilter>().To<OnlyFirstFilter>().InSingletonScope();
			kernel.Bind<IFilter>().To<SingleFilter>().InSingletonScope();

			kernel.Bind<IFilterService>().To<FilterService>().InSingletonScope();


			kernel.Bind<IGetPathService>().To<GetPathService>().InSingletonScope();
		}
	}
}