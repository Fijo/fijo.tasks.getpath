using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Tasks.GetPath.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new GetPathInjectionModule());
		}
	}
}