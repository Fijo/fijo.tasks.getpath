using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Controller;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Service {
	[UsedImplicitly]
	public class FilterService : IFilterService {
		private readonly IFinder<IFilter, FilterRule> _filterFinder;

		public FilterService(IFinder<IFilter, FilterRule> filterFinder) {
			_filterFinder = filterFinder;
		}

		#region Implementation of IFilterService
		public IEnumerable<T> GetFiltered<T>(IEnumerable<T> me, IEnumerable<FilterRule> filterRules) {
			return filterRules.Aggregate(me, (a, x) => _filterFinder.Get(x).Filter(a));
		}
		#endregion
	}
}