using System.Collections.Generic;
using System.Linq;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Service {
	[UsedImplicitly]
	public class GetPathService : IGetPathService {
		private readonly IFilterService _filterService;

		public GetPathService(IFilterService filterService) {
			_filterService = filterService;
		}

		#region Implementation of IGetPathService
		public string Get(IEnumerable<FilterRule> filterRules, IEnumerable<string> fileNames, bool allowEmpty) {
			return GetFile(allowEmpty, GetFilteredFileNames(filterRules, fileNames));
		}

		private string GetFile(bool allowEmpty, IEnumerable<string> filteredFileNames) {
			return allowEmpty ? filteredFileNames.FirstOrDefault() : filteredFileNames.First();
		}

		private IEnumerable<string> GetFilteredFileNames(IEnumerable<FilterRule> filterRules, IEnumerable<string> fileNames) {
			return _filterService.GetFiltered(fileNames, filterRules);
		}
		#endregion
	}
}