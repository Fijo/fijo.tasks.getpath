using System.Collections.Generic;
using System.Linq;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Filters {
	[UsedImplicitly]
	public class SingleFilter : IFilter {
		#region Implementation of IFilter
		public FilterRule For { get { return FilterRule.Single; } }
		public IEnumerable<T> Filter<T>(IEnumerable<T> me) {
			return me.Single().IntoEnumerable();
		}
		#endregion
	}
}