﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Filters {
	[UsedImplicitly]
	public class OnlyFirstFilter : IFilter {
		#region Implementation of IFilter
		public FilterRule For { get { return FilterRule.OnlyFirst; } }
		public IEnumerable<T> Filter<T>(IEnumerable<T> me) {
			return me.Take(1);
		}
		#endregion
	}
}