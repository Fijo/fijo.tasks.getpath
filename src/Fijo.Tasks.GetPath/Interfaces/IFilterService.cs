using System.Collections.Generic;
using Fijo.Tasks.GetPath.Enums;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Interfaces {
	public interface IFilterService {
		[NotNull] IEnumerable<T> GetFiltered<T>([NotNull] IEnumerable<T> me, [NotNull] IEnumerable<FilterRule> filterRules);
	}
}