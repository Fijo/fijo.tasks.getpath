using System.Collections.Generic;
using Fijo.Tasks.GetPath.Enums;
using JetBrains.Annotations;

namespace Fijo.Tasks.GetPath.Interfaces {
	public interface IFilter {
		FilterRule For { get; }
		[NotNull] IEnumerable<T> Filter<T>([NotNull] IEnumerable<T> me);
	}
}