using System.Collections.Generic;
using Fijo.Tasks.GetPath.Enums;

namespace Fijo.Tasks.GetPath.Interfaces {
	public interface IGetPathService {
		string Get(IEnumerable<FilterRule> filterRules, IEnumerable<string> fileNames, bool allowEmpty);
	}
}