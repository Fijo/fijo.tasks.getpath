﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Tasks.GetPath.Enums;
using Fijo.Tasks.GetPath.Interfaces;
using Fijo.Tasks.GetPath.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using NAnt.Core;
using NAnt.Core.Attributes;
using NAnt.Core.Types;

namespace Fijo.Tasks.GetPath
{
	[PublicAPI]
	[TaskName("GetPath")]
	public class GetPath : Task {
		private readonly IGetPathService _getPathService;

		[TaskAttribute("PropName", Required = true)]
		[StringValidator(AllowEmpty = false)]
		[PublicAPI]
		[About("PropertyName")]
		public string PropName { get; set; }

		[PublicAPI]
		[TaskAttribute("Filters", Required = false)]
		public string Filters { get; set; }
		
		[BuildElement("Input", Required = true)]
		[PublicAPI]
		public FileSet Input { get; set; }
		
		[TaskAttribute("AllowEmpty", Required = false)]
		[PublicAPI]
		[Note("default: false")]
		public bool AllowEmpty { get; set; }

		public GetPath() {
			new InternalInitKernel().Init();
			_getPathService = Kernel.Resolve<IGetPathService>();
		}

		protected override void ExecuteTask() {
			var value = _getPathService.Get(GetFilterRules(), GetInputFiles(), AllowEmpty);
			if(Verbose) Console.WriteLine("result: {0}", value);
			SetProp(value);
		}

		private IEnumerable<string> GetInputFiles() {
			return Input.FileNames.Cast<string>();
		}

		private IEnumerable<FilterRule> GetFilterRules() {
			return GetSplitedFilters().Select(x => Enum.Parse(typeof(FilterRule), x)).Cast<FilterRule>();
		}

		private void SetProp(string value) {
			Project.Properties[PropName] = value;
		}

		private IEnumerable<string> GetSplitedFilters() {
			return Filters.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
		}
	}
}
